import java.util.*;

class prg9{
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter Number of Rows : ");
		int row=sc.nextInt();
		for(int i=1;i<=row;i++){
			char ch='A';
			char ch1='a';
			for(int sp=1;sp<=row-i;sp++){
				System.out.print("\t");
			}
			for(int j=1;j<=i*2-1;j++){
				if(i%2==1){

					if(j>=i)
						System.out.print(ch-- +"\t");
					else
						System.out.print(ch++ +"\t");
				}
				else{

					if(j>=i)
						System.out.print(ch1-- +"\t");
					else
						System.out.print(ch1++ +"\t");
				}
			}
			System.out.println();
		}
	}
}
