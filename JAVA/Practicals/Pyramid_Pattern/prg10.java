import java.util.*;

class prg10{
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter Number of Rows : ");
		int row=sc.nextInt();
		char ch=(char)(64+row);
		for(int i=1;i<=row;i++){
			for(int sp=1;sp<=row-i;sp++){
				System.out.print("\t");
			}
			for(int j=1;j<=i*2-1;j++){
				if(j>=i)	
					System.out.print(ch-- +"\t");
				else
					System.out.print(ch++ +"\t");
			}
			System.out.println();
		}
	}
}
