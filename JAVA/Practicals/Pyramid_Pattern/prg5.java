import java.util.*;

class prg5{
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter Number of Rows : ");
		int row=sc.nextInt();
		for(int i=1;i<=row;i++){
			int num=1;
			for(int sp=1;sp<=row-i;sp++){
				System.out.print("\t");
			}
			for(int j=1;j<=i*2-1;j++){
				if(j>=i)
					System.out.print(num-- +"\t");
				else
					System.out.print(num++ +"\t");
			
			}
			System.out.println();
		}
	}
}
