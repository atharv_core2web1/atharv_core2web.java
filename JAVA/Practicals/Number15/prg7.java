import java.util.*;

class prg7{
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter a number ");
		int num = sc.nextInt();
		System.out.print("Reverse of "+num+" is ");
		while(num!=0){
			int rem=num%10;
			num=num/10;
			System.out.print(rem);
		}
		System.out.println();
			
	}
}
