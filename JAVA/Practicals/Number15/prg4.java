import java.util.*;

class prg4{
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter a number ");
		int num = sc.nextInt();
		
		if(num==2||num==3||num==5||num==7){
			System.out.println(num+" is not a composite number");
		}
		else{
		if(num%2==0)
			System.out.println(num+" is a composite number ");
		else if(num%3==0)
			System.out.println(num+" is a composite number ");
		else if(num%5==0)
			System.out.println(num+" is a composite number ");
		else if(num%7==0)
			System.out.println(num+" is a composite number ");
		else
			System.out.println(num+" is not a composite number ");
		}
	}
}
