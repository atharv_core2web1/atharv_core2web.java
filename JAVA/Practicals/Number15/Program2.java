import java.util.*;

class prg1{
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter a number ");
		int num = sc.nextInt();
		System.out.print("Factors of "+num+" are ");
		for(int i=1;i<=num-1;i++){
			if(num%i==0)
				System.out.print(i+",");
		}
		System.out.print(num);
		System.out.println();
	}
}
