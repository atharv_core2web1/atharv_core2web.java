class prg14
{
	public static void main(String [] args)
	{
		int x=14;
		int y=22;

		System.out.println(++x + y++);//15+22=37 x=15,y=23;
		System.out.println(x++ + ++y + ++x + ++x);//15+24+17+18=74 x=18;y=24;
		System.out.println(y++ + ++x + ++x);//24+19+20=63; x=20;y=25;
		System.out.println(x);//20
		System.out.println(y);//25
	}
}
