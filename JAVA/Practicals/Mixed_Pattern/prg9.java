import java.util.*;

class prg9{
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Number of Rows ");
		int row = sc.nextInt();
		for(int i=1;i<=row;i++){
			int num=1;
			char ch=(char)(65+row-i);
			for(int j=1;j<=row-i+1;j++){
				if(i%2==1)
					System.out.print(num++ +" ");
				else
					System.out.print(ch-- +" ");
					
					
			}
			System.out.println();
		}
		
	}
}
