import java.util.*;

class prg10{
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Number ");
		long num = sc.nextLong();
		long rev=0l;
		
		while(num!=0){
			long rem=num%10;
			num=num/10;
			rev=rev*10+rem;	
		}
		while(rev!=0){
			long rem=rev%10;
			rev=rev/10;
			if(rem%2==1)
				System.out.print(rem*rem+",");
		}
		System.out.println();
		
	}
}
