class prg8
{
	public static void main(String [] args)
	{
		float per=47.0f;

		if(per>=75.0)
		{

			System.out.println("Passed : First Class with Distinction");
		}
		else if(per<75.0 && per>=60)
		{

			System.out.println("Passed : First Class");
		}
		else if(per<60.0 && per>=50)
		{

			System.out.println("Passed : Second Class");
		}
		else if(per<50 && per>=40)
		{

			System.out.println("Pass");
		}
		else
		{

			System.out.println("Fail");
		}
	}
}
