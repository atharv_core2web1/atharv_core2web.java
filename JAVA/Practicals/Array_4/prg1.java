import java.util.*;

class prg1{
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Size of Array : ");
		int size=sc.nextInt();
		int sum=0;
		int arr[] =new int[size];
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
			sum+=arr[i];	
		}
		float avg=sum/size;
		System.out.println("Array Elements' average is : "+avg);
	
	}
}
