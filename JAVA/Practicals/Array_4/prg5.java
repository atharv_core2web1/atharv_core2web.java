import java.util.*;

class prg5{
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Size of Array : ");
		int size=sc.nextInt();
		int arr[] =new int[size];
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();	
		}
		int temp=arr[0];
		for(int i=0;i<arr.length/2;i++){	
			temp=arr[i];
			arr[i]=arr[size-i-1];
			arr[size-i-1]=temp;
		}
		System.out.print("Reversed Array : " );
		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i]+" ");
		}
		System.out.println();
	}
}
