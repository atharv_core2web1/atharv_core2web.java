import java.util.*;

class prg2{
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Size of Array : ");
		int size=sc.nextInt();
		int sum=0;
		int arr[] =new int[size];
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();	
		}
		int max=arr[0];
		int min=arr[0];
		for(int i=0;i<arr.length;i++){	
			if(arr[i]>max)
				max=arr[i];
			if(arr[i]<min)
				min=arr[i];
		}
		int dif=max-min;
		System.out.println("The difference between the minimum and maximum elements is : "+dif);
	}
}
