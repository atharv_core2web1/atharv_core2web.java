import java.util.*;

class prg8{
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Size of Array : ");
		int size=sc.nextInt();
		char arr[] =new char[size];
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.next().charAt(0);	
		}
		System.out.println("Enter Character to check : ");
		char ch=sc.next().charAt(0);
		int count=0;
		for(int i=0;i<arr.length;i++){	
			if(arr[i]==ch)
				count++;
		}
		System.out.println(ch+" occurs "+count+" time in the given array ");

	}
}
