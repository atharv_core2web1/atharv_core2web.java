import java.util.*;

class prg9{
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Size of Array : ");
		int size=sc.nextInt();
		char arr[] =new char[size];
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.next().charAt(0);	
		}
		for(int i=0;i<arr.length;i++){	
			if(arr[i]>=97 && arr[i]<=122)
				System.out.print(arr[i]+" ");
			else
				System.out.print("# ");
		}
		System.out.println();

	}
}
