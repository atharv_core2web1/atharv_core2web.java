import java.util.*;

class prg4{
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Size of Array : ");
		int size=sc.nextInt();
		int arr[] =new int[size];
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();	
		}
		System.out.println("Enter Number to check : ");
		int num=sc.nextInt();
		int count=0;
		for(int i=0;i<arr.length;i++){	
			if(arr[i]==num)
				count++;
		}
		if(count>2)
			System.out.println(num+" occurs more than 2 times in the array");
		else if(count==2)
			System.out.println(num+" occurs 2 times in the array");
	
		else if(count==1)
			System.out.println(num+" occurs 1 times in the array");
		else
			System.out.println(num+" is not found in the array");
	}
}
