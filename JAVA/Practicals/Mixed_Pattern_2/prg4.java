import java.util.*;

class prg4{
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Number of Rows : ");
		int row=sc.nextInt();
		char ch='A';
		char ch1='a';
		char temp='A';
		char temp1='a';
		for(int i=1;i<=row;i++){
			 ch=temp;
			 ch1=temp1;
			for(int sp=1;sp<i;sp++){
				System.out.print("\t");
			}
			for(int j=1;j<=row-i+1;j++){
				if(row%2==1){
					if(j==2)
						temp=ch;
					System.out.print(ch++ +"\t");
				}
				else{
					if(j==2)
						temp1=ch1;
					System.out.print(ch1++ +"\t");
				}
			}
			System.out.println();
		}

	}
}
