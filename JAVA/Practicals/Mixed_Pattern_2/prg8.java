import java.util.*;

class prg8{
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Number of Rows : ");
		int row=sc.nextInt();
		int temp=row;
		for(int i=1;i<=row;i++){
			int num=temp;
			for(int sp=1;sp<=row-i;sp++){
				System.out.print("\t");
			}
			for(int j=1;j<=i;j++){
				System.out.print(num++ +"\t");
			}
			for(int k=1;k<i;k++){
				System.out.print(num++ +"\t");
			}
			 temp--;
			System.out.println();
		}

	}
}
