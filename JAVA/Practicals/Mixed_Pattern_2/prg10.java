import java.util.*;

class prg10{
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Number of Rows : ");
		int row=sc.nextInt();
		int temp=1;
		for(int i=1;i<=row;i++){
			int num=temp;
			for(int sp=1;sp<=row-i;sp++){
				System.out.print("\t");
			}
			for(int j=1;j<=i+i-1;j++){
				if(j>=i){
					System.out.print(num++ +"\t");
				}
				else
					System.out.print(num-- +"\t");
			}
			 temp++;
			System.out.println();
		}

	}
}
