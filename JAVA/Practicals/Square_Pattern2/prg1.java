import java.util.*;

class prg1{
	public static void main(String [ ]args){
		Scanner sc= new Scanner(System.in);
		System.out.println("Enter Number of Rows : ");
		int row=sc.nextInt();
		char ch=(char)(64+row);
		char ch1=(char)(96+row);
		for(int i=1;i<=row;i++){
			for(int j=1;j<=row;j++){
				if(j==1)
					System.out.print(ch+"\t");
				else
					System.out.print(ch1+"\t");
				ch++;
				ch1++;
			}
			System.out.println();
		}
	}
}
