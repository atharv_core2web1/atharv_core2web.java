import java.util.*;

class prg1{
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Number of Rows ");
		int row = sc.nextInt();

		for(int i=1;i<=row;i++){
			int num=i;
			for(int j=i;j<=row;j++){
				System.out.print(num++ +" ");
			}
			System.out.println();
		}
	}
}
