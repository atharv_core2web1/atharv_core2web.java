import java.util.*;

class prg7{
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Number of Rows ");
		int row = sc.nextInt();
		for(int i=1;i<=row;i++){
			char ch1=(char)(96+row-i);
			int num=row-i+1;
			for(int j=1;j<=row-i+1;j++){
				if(j%2==1){
					System.out.print(num +" ");
					num-=2;
				}
				else{
					System.out.print(ch1 +" ");
					ch1-=2;
				}
			}
			System.out.println();
		}
	}
}
