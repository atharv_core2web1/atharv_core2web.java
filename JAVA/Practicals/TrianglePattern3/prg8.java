import java.util.*;

class prg8{
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Number of Rows ");
		int row = sc.nextInt();
		for(int i=1;i<=row;i++){
			char ch1=(char)(64+row-i+1);
			int num=row-i+1;
			for(int j=1;j<=row-i+1;j++){
				if(i%2==1){
					System.out.print(num-- +" ");
				}
				else{
					System.out.print(ch1-- +" ");
				}
			}
			System.out.println();
		}
	}
}
