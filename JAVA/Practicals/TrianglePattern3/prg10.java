import java.util.*;

class prg10{
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Number of Rows ");
		int row = sc.nextInt();

		for(int i=1;i<=row;i++){
			char ch=(char)(64+row-i+1);
			char ch1=(char)(96+row-i+1);
			for(int j=1;j<=row-i+1;j++){
				if(row%2==1){
					if(i%2==1)
						System.out.print(ch1-- +" ");
					else
						System.out.print(ch-- +" ");

				}
				else{
					if(i%2==1)
						System.out.print(ch-- +" ");
					else
						System.out.print(ch1-- +" ");
					
				}
				
			}
			System.out.println();
		}
	}
}
