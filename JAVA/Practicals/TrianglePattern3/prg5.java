import java.util.*;

class prg5{
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Number of Rows ");
		int row = sc.nextInt();
		for(int i=1;i<=row;i++){
			char ch=65;
			char ch1=97;
			for(int j=i;j<=row;j++){
				if(i%2==1)
					System.out.print(ch++ +" ");
				else
					System.out.print(ch1++ +" ");

			}
			System.out.println();
		}
	}
}
