import java.util.*;

class prg2{
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Number of Rows ");
		int row = sc.nextInt();
		int num=2;
		for(int i=1;i<=row;i++){
			for(int j=i;j<=row;j++){
				System.out.print(num +" ");
				num+=2;
			}
			System.out.println();
		}
	}
}
