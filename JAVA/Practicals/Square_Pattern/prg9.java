import java.util.*;

class prg9{
	public static void main(String [] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Number of Rows : ");
		int row=sc.nextInt();

		for(int i=1;i<=row;i++){
			for(int j=1;j<=row;j++){
				if(i%2==1){
					if(j%2==1)
						System.out.print(j*2 +"\t");
					else
						System.out.print(j*2+j+"\t");
				}
				else{
					if(j%2==1)
						System.out.print(j*3+"\t");	
					else
						System.out.print(j*2+"\t");
				}	
			}
			System.out.println();
		}
	}
}
