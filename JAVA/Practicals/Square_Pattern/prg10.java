import java.util.*;

class prg10{
	public static void main(String [] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Number of Rows : ");
		int row=sc.nextInt();
		for(int i=1;i<=row;i++){
			int num=row;
			for(int j=1;j<=row;j++){
				if(i%2==1){
					if(j%2==1)
						System.out.print(num-- +"\t");
					else
						System.out.print((char)(64+num--)+"\t");
				}
				else{
						System.out.print((char)(64+num--)+"\t");	
				}	
			}
			System.out.println();
		}
	}
}
