import java.util.*;

class prg4{
	public static void main(String [] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Number of Rows : ");
		int row=sc.nextInt();
		int num=row;
		char ch=(char)(64+row);
		for(int i=1;i<=row;i++){
			for(int j=1;j<=row;j++){
				if(num%row==0)
					System.out.print(ch +"\t");
				else
					System.out.print(num+ "\t");
				num++;
				ch++;
			}
			System.out.println();
		}
	}
}
