import java.util.*;

class prg1{
	public static void main(String [] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Number of Rows : ");
		int row=sc.nextInt();
		int num=row;

		for(int i=1;i<=row;i++){
			char ch=(char)(64+row);
			for(int j=1;j<=row;j++){
				if(i%2==1)
					System.out.print(ch-- +"\t");
				else
					System.out.print(num+"\t");	
			}
			System.out.println();
		}
	}
}
