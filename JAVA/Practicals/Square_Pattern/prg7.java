import java.util.*;

class prg7{
	public static void main(String [] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Number of Rows : ");
		int row=sc.nextInt();
		int num=row;
		char ch='A';
		for(int i=1;i<=row;i++){
			for(int j=1;j<=row;j++){
				if(num%2==1)
					System.out.print(ch +"\t");
				else
					System.out.print(num+ "\t");
				num++;
			}ch++;
			System.out.println();
		}
	}
}
