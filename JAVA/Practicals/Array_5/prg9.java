import java.util.*;

class prg9{
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the Number :  ");
		long num=sc.nextInt();
		System.out.println();
		long rev=0l;
		int count=0;
		long rem=0l;
		while(num!=0){
			 rem=num%10;
			num/=10;
			rev=rev*10+rem;
			count++;
		}

		long arr[]=new long[count];
		int i=0;
		while(i<count){
			 rem=rev%10;
			 rev/=10;
			 arr[i]=rem+1;
			 System.out.print(arr[i]+" ");
			 i++;
		}
		System.out.println();
		
	}
}
