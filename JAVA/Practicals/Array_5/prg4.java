import java.util.*;

class prg4{
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter Size of the array : ");
		int size=sc.nextInt();
		System.out.println();
		int arr[]=new int[size];
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		int flag=0;
		for(int i=0;i<size;i++){
			for(int j=0;j<size;j++){
				if(i==j)
					continue;
				if(arr[i]==arr[j]){
					System.out.println("First duplicate element present at index "+i);
					flag=1;
					break;
					
				}

			}
			if(flag==1)
				break;
		}
	}
}
