import java.util.*;

class prg3{
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter Size of the array : ");
		int size=sc.nextInt();
		System.out.println();
		int arr[]=new int[size];
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		int flag=0;
		for(int i=0;i<size/2;i++){
			if(arr[i]!=arr[size-i-1]){
				flag=1;
			}

		}
		if(flag==0)
			System.out.println("The given array is palindrome array ");
		else
			System.out.println("The given array is not palindrome array ");
	}
}
