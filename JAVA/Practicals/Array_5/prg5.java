import java.util.*;

class prg5{
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter Size of the array : ");
		int size=sc.nextInt();
		System.out.println();
		int arr[]=new int[size];
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		int num=arr[0];
		int count=0;
		for(int i=0;i<size;i++){
			count=0;
			num=arr[i];
			while(num!=0){
				num/=10;
				count++;
			}
			System.out.print(count+" ");
		}
		System.out.println();
	}
}
