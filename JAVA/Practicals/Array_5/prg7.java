import java.util.*;

class prg7{
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter Size of the array : ");
		int size=sc.nextInt();
		System.out.println();
		int arr[]=new int[size];
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		System.out.print("Composite numbers in an array are : ");
		for(int i=0;i<size;i++){
			int count=0;
			for(int j=1;j<=arr[i];j++){
				if(arr[i]%j==0)
					count++;
			}
			if(count>2){
				System.out.print(arr[i]+" ");
			}

		}System.out.println();
	}
}
