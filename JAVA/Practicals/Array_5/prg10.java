import java.util.*;

class prg10{
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter Size of the array : ");
		int size=sc.nextInt();
		System.out.println();
		int arr[]=new int[size];
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		int num=arr[0];
		int fact=1;
		for(int i=0;i<size;i++){
			num=arr[i];
			fact=1;
			while(num>1){
				fact*=num;
				num--;
			}
			System.out.print(fact+" ");

		}
		System.out.println();
	}
}
