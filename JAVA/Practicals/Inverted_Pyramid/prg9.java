import java.util.*;

class prg9{
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter Number of Rows : ");
		int row = sc.nextInt();
		System.out.println();
		int num=1;
		for(int i=1;i<=row;i++){
			for(int sp=1;sp<i;sp++){
				System.out.print("\t");
			}
			for(int j=1;j<=(row-i+1)*2-1 ;j++){
				if(j%2==1)
					System.out.print("1"+"\t");
				else
					System.out.print("0"+"\t");
			}
			
			System.out.println();
		}
	}
}
