import java.util.*;

class prg4{
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter Number of Rows : ");
		int row = sc.nextInt();
		System.out.println();
		for(int i=1;i<=row;i++){
			int num=1;
			for(int sp=1;sp<i;sp++){
				System.out.print("\t");
			}
			for(int j=1;j<=(row-i+1)*2-1 ;j++){
				System.out.print(num+"\t");
				num++;
			}
			System.out.println();
		}
	}
}
