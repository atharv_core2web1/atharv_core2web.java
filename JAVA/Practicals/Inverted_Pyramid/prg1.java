import java.util.*;

class prg1{
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Number of Rows : ");
		int row = sc.nextInt();

		for(int i=1;i<=row;i++){
			for(int sp=1;sp<i;sp++){
				System.out.print("\t");
			}
			for(int j=1;j<=(row-i+1)*2-1 ;j++){
				System.out.print("1\t");
			}
			System.out.println();
		}
	}
}
