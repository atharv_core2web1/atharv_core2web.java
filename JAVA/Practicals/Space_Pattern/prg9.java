import java.util.*;

class prg9{
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter Number of Rows : ");
		int row = sc.nextInt();
		

		for(int i=1;i<=row;i++){
			char ch=(char)(64+row);
			for(int sp=1;sp<i;sp++){
				System.out.print("\t");
			}
			for(int j=1;j<=row-i+1;j++){
				System.out.print(ch-- +"\t");
			}
			
			System.out.println();
		}
	}
}
