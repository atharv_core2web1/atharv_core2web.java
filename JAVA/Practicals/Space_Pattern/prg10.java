import java.util.*;

class prg10{
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter Number of Rows : ");
		int row = sc.nextInt();
		int n=row;

		for(int i=1;i<=row;i++){
			int ch=64+i;
			for(int sp=1;sp<i;sp++){
				System.out.print("\t");
			}
			for(int j=1;j<=row-i+1;j++){
				if(n%2==0){
					if(j%2==1)
						System.out.print(ch++ +"\t");
					else
						System.out.print((char)(ch++)+"\t");
				}
				else{
					if(j%2==1)
						System.out.print((char)(ch++)+"\t");
					else
						System.out.print(ch++ +"\t");
				}
			}n--;
			
			System.out.println();
		}
	}
}
