import java.util.*;

class prg4{
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter Size of the Array : ");
		int size=sc.nextInt();
		System.out.println();
		int arr1[]=new int[size];
		System.out.print("Enter the Elements of the Array 1 : ");
		for(int i=0;i<arr1.length;i++){
			arr1[i]=sc.nextInt();
		}
		int arr2[]=new int[size];
		System.out.print("Enter the Elements of the Array 2 : ");
		for(int i=0;i<size;i++){
			arr2[i]=sc.nextInt();
		}
		System.out.print("Common elements in the given arrays are : ");
		for(int i=0;i<size;i++){
			for(int j=0;j<size;j++){
				if(arr1[i]==arr2[j])
					System.out.print(arr1[i]+" ");
			}
		}
		System.out.println();

	}

}
