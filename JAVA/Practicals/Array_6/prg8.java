import java.util.*;

class prg8{
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter Size of the Array : ");
		int size=sc.nextInt();
		System.out.println();
		char arr[]=new char[size];
		System.out.print("Enter the Elements of the Array : ");
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.next().charAt(0);
		}
		System.out.print("Before Reversing : ");
		for(int i=0;i<arr.length;i+=2){
			System.out.print(arr[i]+" ");
		}
		System.out.println();
		char temp=arr[0];
		for(int i=0;i<arr.length/2;i++){
			temp=arr[i];
			arr[i]=arr[size-i-1];
			arr[size-i-1]=temp;
		}
		System.out.print("After Reversing : ");
		for(int i=0;i<arr.length;i+=2){
			System.out.print(arr[i]+" ");
		}
		System.out.println();

	}

}
