import java.util.*;

class prg6{
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter Size of the Array : ");
		int size=sc.nextInt();
		int arr[]=new int[size];
		System.out.print("Enter the Elements of the Array  : ");
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		System.out.print("Enter key ");
		int key=sc.nextInt();
		for(int i=0;i<size;i++){
			if(arr[i]%key==0){
				System.out.println("An element multiple of "+key+" found at index : "+i);
			}
		}
		System.out.println();

	}

}
