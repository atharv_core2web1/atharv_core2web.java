import java.util.*;

class prg2{
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter Size of the Array : ");
		int size=sc.nextInt();
		System.out.println();
		int arr[]=new int[size];
		System.out.print("Enter the Elements of the Array : ");
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		int prime=0;
		int p=0;
		for(int i =0;i<size-1;i++){
			int count=0;
			for(int j=1;j<=arr[i];j++){
				if(arr[i]%j==0){
					count++;
				}
			}
			if(count==2){
				prime+=arr[i];
				p++;
			}
		}
			System.out.println("Sum of all prime numbers is "+prime+" and count of prime numbers in the given array is "+p );
	}

}
