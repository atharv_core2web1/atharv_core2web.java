import java.util.*;

class prg7{
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter Size of the Array : ");
		int size=sc.nextInt();
		System.out.println();
		int arr[]=new int[size];
		System.out.print("Enter the Elements of the Array : ");
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		for(int i=0;i<arr.length;i++){
			if(arr[i]>=65 && arr[i]<=90)
				System.out.print((char)(arr[i])+" ");
			else
				System.out.print(arr[i]+" ");
		}
	}

}
