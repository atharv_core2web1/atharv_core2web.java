import java.util.*;

class prg3{
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter Size of the Array : ");
		int size=sc.nextInt();
		System.out.println();
		int arr[]=new int[size];
		System.out.print("Enter the Elements of the Array : ");
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		System.out.print("Enter key : ");
		int key=sc.nextInt();
		int count=0;
		for(int i=0;i<size;i++){
			if(arr[i]==key){
				count++;
			}
		}
		if(count==0)
			System.out.println("Element not found ");
		else{
			for(int i=0;i<size;i++){
				if(count>2){
					if(arr[i]==key)
						System.out.print(key*key*key+" ");
					else
						System.out.print(arr[i]+" ");
				}
				else
					System.out.print(arr[i]+" ");
			}
		}

	}

}
