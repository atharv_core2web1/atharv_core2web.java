import java.util.*;

class prg9{
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter Size of the Array : ");
		int size=sc.nextInt();
		System.out.println();
		int arr[]=new int[size];
		System.out.print("Enter the Elements of the Array : ");
		for(int i=0;i<size;i++){
			arr[i]=sc.nextInt();
		}
		int num=0;
		int count=0;
		int temp=num;
		int rem=0;
		int rev=0;
		for(int i=0;i<size;i++){
			 num=arr[i];
		         temp=num;
			 rev=0;
			while(num>0){
				 rem=num%10;
				num/=10;
				rev=10*rev+rem;
			}	
			if(rev==arr[i]){
				count++;
			}
		}
		System.out.print("Count of palindrome elements is : "+count);
		System.out.println();
	}

}
