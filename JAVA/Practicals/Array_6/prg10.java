import java.util.*;

class prg10{
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter Size of the Array : ");
		int size=sc.nextInt();
		System.out.println();
		int arr[]=new int[size];
		System.out.print("Enter the Elements of the Array : ");
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		int flag=0;
		for(int i =0;i<size-1;i++){
			if(arr[i]<arr[i+1]){
				flag=1;
				break;
			}
		}
		if(flag==0)
			System.out.println("Given array is in descending order " );
		else
			System.out.println("Given array is not in descending order ");
	}

}
