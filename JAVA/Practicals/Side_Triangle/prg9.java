import java.util.*;

class prg9{
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter Number of Rows : ");
		int row=sc.nextInt();
		int col=0;
		int num=row;
		int temp=1;
		for(int i=1;i<row*2;i++){
			if(i<=row)
				col=row-i;
			else
				col=i-row;

			for(int sp=1;sp<=col;sp++){
				System.out.print("\t");
			}
			if(i<=row){
				col=i;
				num--;
			}
			else{
				col=row*2-i;
				num++;
			}
			temp=num;
			for(int j=1;j<=col;j++){
				System.out.print(temp++ +"\t");
			}
			System.out.println();
		}
	}
}
