import java.util.*;

class prg3{
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter Number of Rows : ");
		int row=sc.nextInt();
		int col=0;
		int num=1;
		for(int i=1;i<row*2;i++){
			if(i<=row)
				col=i;
			else
				col=row*2-i;
			num=col;
			for(int j=1;j<=col;j++){
				System.out.print(num-- +"\t");
			}
			System.out.println();
		}
	}
}
