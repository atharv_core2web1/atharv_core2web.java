import java.util.*;

class prg10{
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter Number of Rows : ");
		int row=sc.nextInt();
		int col=0;
		char ch=(char)(65+row);
		char temp;
		for(int i=1;i<row*2;i++){
			if(i<=row)
				col=row-i;
			else
				col=i-row;

			for(int sp=1;sp<=col;sp++){
				System.out.print("\t");
			}
			if(i<=row){
				col=i;
				ch--;
			}
			else{
				col=row*2-i;
				ch++;
			}
			temp=ch;
			for(int j=1;j<=col;j++){
				System.out.print(temp++ +"\t");
			}
			System.out.println();
		}
	}
}
