import java.util.*;

class prg1{
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter Number of Rows : ");
		int row=sc.nextInt();
		for(int i=1;i<=row*2-1;i++){
			for(int j=1;j<=row;j++){
				if(i<=row){
					if(j<=i)
						System.out.print("*\t");
				}else{
					if(j<=row*2-i)
						System.out.print("*\t");
				}	
			}
			System.out.println();
		}
	}
}
