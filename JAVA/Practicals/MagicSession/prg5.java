import java.util.*;

class Demo{
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter a Number : ");

		int num = sc.nextInt();

		if(num%16==0)
			System.out.println(num+" is in the table of 16");
		else
			System.out.println(num+" is not in the table of 16");

	}

}
