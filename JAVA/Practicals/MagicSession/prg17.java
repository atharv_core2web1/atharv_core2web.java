import java.util.*;

class Demo{
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter row ");

		int row = sc.nextInt();
		for(int i=1;i<=row;i++){
			char ch=(char)(96+row);
			for(int j=1;j<=row;j++){
				System.out.print(ch+" ");
				ch--;
			}	
			System.out.println();
		}

	}
}
