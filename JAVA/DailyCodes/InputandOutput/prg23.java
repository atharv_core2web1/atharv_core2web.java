import java.io.*;

class InputDemo{
	public static void main(String [] args)throws IOException{
		InputStreamReader isr=new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(isr);
		System.out.println("Enter Your Name : ");
		String name = br.readLine();
		System.out.println("Enter Company Name : ");
		String compname = br.readLine();
		System.out.println("Enter Employee Name : ");
		String empname = br.readLine();
		System.out.println("Name : "+name);
		System.out.println("Company Name : "+compname);
		System.out.println("Employee Name : "+empname);


}
}
