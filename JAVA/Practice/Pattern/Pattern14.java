import java.util.*;
class p14{
	public static void main(String [] args){
		Scanner sc=new Scanner(System.in);

		System.out.print("Enter Number Of Rows : ");
		int row=sc.nextInt();
		int num=1;
		for(int i=1;i<=row;i++){
			for(int j=row;j>=1;j--){
				if(j>=i)
					System.out.print("* ");
			}

			System.out.println();
		}	
	}
}
